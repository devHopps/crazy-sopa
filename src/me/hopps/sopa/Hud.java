package me.hopps.sopa;

import org.lwjgl.Sys;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class Hud {
	Image Ammu;
	int munition, points;
	long lasttimedrawreload, lasttimesettrue, counter;
	UnicodeFont pointsFont, reloadFont;
	boolean started;
	int time, numberofshots;
	
	public Hud() {
		
	}

	@SuppressWarnings("unchecked")
	public void init(GameContainer gc) throws SlickException {
		
		SpriteSheet Sprites = new SpriteSheet("res/img/sprites.png", 16, 16);
		
		gc.setMouseCursor(Sprites.getSprite(1, 0), 8, 8);
		
		Ammu = Sprites.getSprite(0, 0);
		
		munition = 15;
		time = 30;
		
		String fontPath = "res/fonts/font.ttf";
		pointsFont = new UnicodeFont(fontPath , 32, false, false);
		pointsFont.addAsciiGlyphs();
		pointsFont.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		pointsFont.loadGlyphs();
		
		reloadFont = new UnicodeFont(fontPath , 32, false, false);
		reloadFont.addAsciiGlyphs();
		reloadFont.getEffects().add(new ColorEffect(java.awt.Color.RED));
		reloadFont.loadGlyphs();
	}
	
	public void reinit(GameContainer gc) throws SlickException {
		munition = 15;
		time = 30;
		numberofshots = 0;
		points = 0;
	}
	
	public void update(GameContainer gc, int delta) throws SlickException {
		if(started) {
			if(getTime()-counter > 1000) {
				time--;
				counter = getTime();
			}
		}
	}
	
	public void render(GameContainer gc, Graphics g) throws SlickException {
		for(int i = 0; i < munition; i++) {
			if(munition > i) {
				Ammu.draw(16*i+8*i, 535, 2);
			}
		}
		
		if(munition < 6) {
			boolean drawReload;
			if(getTime()-lasttimedrawreload > 500) {
				drawReload = true;
			} else {
				drawReload = false;
			}
			if(getTime()-lasttimedrawreload > 1000 ) {
				lasttimedrawreload = getTime();
				drawReload = false;
			}
			if(drawReload) {
				reloadFont.drawString(250, 400, "Space to reload");
			}
		}
		pointsFont.drawString(425, 535, time +"");
		pointsFont.drawString(525, 535, "Points:");
		int Textwidth = pointsFont.getWidth("" + points);
		pointsFont.drawString(792 - Textwidth, 535, "" + points);
	}
	
	long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public void shoot() {
		if(started) {
			if(munition > 0) {
				numberofshots++;
			}
			munition--;
		}
	}
	
	public void reload() {
		if(munition < 15) {
			points -= 100 + (munition*10);
			munition = 15;
		}
	}
	
	public int getMunition() {
		return munition;
	}

	public void addPoints(int points2) {
		points += points2;
	}

	public void start() {
		started = true;
	}

	public int getGameTime() {
		return time;
	}

	public void stop() {
		started = false;
	}
	
}
