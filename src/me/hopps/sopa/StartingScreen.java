package me.hopps.sopa;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

public class StartingScreen {
	Image Background;
	UnicodeFont Headline, Text;
	boolean hidden;

	@SuppressWarnings("unchecked")
	public void init(GameContainer gc) throws SlickException {
		Background = new Image("res/img/back.png");
		
		String fontPath = "res/fonts/font.ttf";
		Headline = new UnicodeFont(fontPath , 36, false, false);
		Headline.addAsciiGlyphs();
		Headline.getEffects().add(new ColorEffect(java.awt.Color.RED));
		Headline.loadGlyphs();
		
		Text = new UnicodeFont(fontPath , 12, false, false);
		Text.addAsciiGlyphs();
		Text.getEffects().add(new ColorEffect(java.awt.Color.WHITE));
		Text.loadGlyphs();
	}
	
	public void update(GameContainer gc, int delta) {
		
	}

	public void render(GameContainer gc, Graphics g) {
		if(!hidden) {
			Background.draw(0, 0);
			Background.draw(0, 150);
			Background.draw(0, 150);
			Background.draw(0, 300);
			Headline.drawString(290, 160, "Crazy SOPA");
			Text.drawString(200, 200, "The lobbyists are trying to flee! Kill them!");
			Text.drawString(200, 215, "Kill them!");
			Text.drawString(200, 230, "------Hints:------");
			Text.drawString(200, 245, "-> Press 'SPACE' to reload your weapon");
			Text.drawString(200, 260, "-> Reloading costs 100 points + 10*Number of Ammunition left");
			Text.drawString(200, 275, "-> You get points for shooting the planes(far away = better)");
			Text.drawString(200, 290, "-> You have got 30 seconds to kill as many as possible");
			Headline.drawString(210, 340, "Press 'S' to start!");
		}
	}

	public void hide() {
		hidden = true;
	}

	public void showWinningScreen(int numshots, int numkills, int points) {
		if(numshots == 0) {
			numshots = 1;
		}
		double numbshots = numshots;
		double numbkills = numkills;
		Background.draw(0, 0);
		Background.draw(0, 150);
		Background.draw(0, 150);
		Background.draw(0, 300);
		Headline.drawString(210, 160, "Congratulations!");
		Text.drawString(200, 200, "You successfully shot " + numkills + " planes");
		Text.drawString(200, 215, "Therefore you got " + points + " points");
		Text.drawString(200, 230, "You tried to shoot " + numshots +" times");
		Text.drawString(200, 245, "Which results in the accuracy of " + Math.round((numbkills/numbshots)*100) + " %");
		Text.drawString(200, 260, "Do you want to play again?");
		
		Headline.drawString(100, 340, "Press 'R' to reload the game!");
	}

	

}
