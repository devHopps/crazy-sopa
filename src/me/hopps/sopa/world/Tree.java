package me.hopps.sopa.world;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Tree {
	Image Trees;
	
	public Tree() {
		
	}
	
	public void init(GameContainer gc) throws SlickException {
		Trees = new Image("res/img/trees.png");
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		
	}
	
	public void render(GameContainer gc, Graphics g) throws SlickException {
		Trees.draw();
	}

	
}
