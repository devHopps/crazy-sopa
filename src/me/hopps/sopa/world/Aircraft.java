package me.hopps.sopa.world;

import java.util.Random;

import me.hopps.sopa.SoundPlayer;

import org.newdawn.slick.Animation;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Aircraft {
	Image Image;
	int layer, hitx, hity;
	float x,y,speed;
	boolean lefttoright, hit;
	Animation ExploAnim;
	World World;
	int ONE = 1;
	
	public Aircraft(World pWorld) {
		World = pWorld;
	}
	
	public void init(GameContainer gc) throws SlickException {
		ExploAnim = new Animation();
		SpriteSheet ExploSprites = new SpriteSheet("res/img/exploanim.png", 64, 64);
		ExploAnim.addFrame(ExploSprites.getSprite(0, 0), 120);
		ExploAnim.addFrame(ExploSprites.getSprite(1, 0), 120);
		ExploAnim.addFrame(ExploSprites.getSprite(2, 0), 120);
		ExploAnim.addFrame(ExploSprites.getSprite(3, 0), 120);
		ExploAnim.addFrame(ExploSprites.getSprite(0, 1), 120);
		ExploAnim.addFrame(ExploSprites.getSprite(0, 2), 120);
		ExploAnim.setLooping(false);
		ExploAnim.setAutoUpdate(true);
		
		Random rand = new Random();
		lefttoright = rand.nextBoolean();
		Image = new Image("res/img/aircraft" + lefttoright +".png");

		speed = rand.nextFloat();
		if(speed < 0.3) {
			speed = 0.3f;
		} else if(speed > 0.5) {
			speed = 0.5f;
		}
		
		layer = rand.nextInt(100);
		if(layer > 0 && layer <= 10) {
			layer = 1;
		} else if(layer > 10 && layer <= 40) {
			layer = 2;
		} else if(layer > 40 && layer <= 70) {
			layer = 3;
		} else {
			layer = 4;
		}
		
		if(layer == 1) {
			y = rand.nextInt(500-Image.getWidth());
		}
		if(layer == 2) {
			y = rand.nextInt(180-Image.getWidth());
		}
		if(layer == 3) {
			y = rand.nextInt(180-Image.getWidth());
		}
		if(layer == 4) {
			y = rand.nextInt(180-Image.getWidth());
		}
		
		if(lefttoright == true) {
			x -= Image.getWidth();
		} else {
			x = 800;
		}
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		if(hit == false) {
			if(lefttoright == true) {
				x += 0.25*delta-(0.1*layer);
			} else {
				x -= 0.25*delta-(0.1*layer);
			}
		} else {
			y += 0.6*delta-(0.1*layer);
			if(Image.getRotation() < 90 && Image.getRotation() > -90) {
				if(lefttoright) {
					Image.rotate(2);
				} else {
					Image.rotate(-2);
				}
			}
		}
	}
	
	public void render(GameContainer gc, Graphics g) throws SlickException {
		float zoomlevel = 1.0f;
		if(layer == 1) {
			zoomlevel = 1.0f;
		} else if (layer == 2) {
			zoomlevel /= 2;
		} else if (layer == 3){
			zoomlevel /= 3;
		} else {
			zoomlevel /= 4;
		}
		Image.draw(Math.round(x),Math.round(y), zoomlevel);
		if(hit == true) {
			ExploAnim.draw(hitx, hity);
		}
	}
	
	public void checkShot(int phitx, int phity, SoundPlayer snd) {
		if(World.checkMunition() >= 0) {
			if(layer == 1) {
				if(x < phitx && x+Image.getWidth() > phitx && y < phity && y+Image.getHeight() > phity)  {
					hit = true;
				}
			} else if(layer == 2) {
				if(x < phitx && x+Image.getWidth()/2 > phitx && y < phity && y+Image.getHeight()/2 > phity)  {
					hit = true;
				}
			} else if(layer == 3) {
				if(x < phitx && x+Image.getWidth()/3 > phitx && y < phity && y+Image.getHeight()/3 > phity)  {
					hit = true;
				}
			} else if(layer == 4) {
				if(x < phitx && x+Image.getWidth()/4 > phitx && y < phity && y+Image.getHeight()/4 > phity)  {
					hit = true;
				}
			}
		}
		
		if(hit == true && ONE == 1) {
			snd.PlaySound("exploision");
			ONE--;
			ExploAnim.start();
			hitx = phitx-32;
			hity = phity-32;
			World.addPoints(10+(10*(layer-1)));
		}
	}
	
	public int getLayer() {
		return layer;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		
		return y;
	}
	
	public boolean getHit() {
		if(hit == true && ExploAnim.isStopped()) {
			return true;
		}
		return false;
	}

}
