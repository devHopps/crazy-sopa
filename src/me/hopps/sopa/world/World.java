package me.hopps.sopa.world;

import java.util.ArrayList;

import org.lwjgl.Sys;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import me.hopps.sopa.Hud;
import me.hopps.sopa.SoundPlayer;

public class World {
	ArrayList<Cloud> Clouds;
	ArrayList<Aircraft> Aircrafts;
	Image Background;
	Tree Trees;
	long LastTimeCreated, LastTimeCraftCreated;
	Hud Hud;
	boolean started;
	public int numberofkills;
	SoundPlayer Sounds;
	
	public World(Hud pHud, SoundPlayer snd) {
		Hud = pHud;
		Sounds = snd;
	}
	
	public void init(GameContainer gc) throws SlickException {
		Clouds = new ArrayList<Cloud>();
		Aircrafts = new ArrayList<Aircraft>();
		
		Background = new Image("res/img/background.png");
		Trees = new Tree();
		Trees.init(gc);
		
		this.generateSomeClouds(gc);
	}
	
	public void reinit(GameContainer gc) throws SlickException {
		numberofkills = 0;
	}
	

	private void generateSomeClouds(GameContainer gc) throws SlickException {
		for(int i = 0; i < 25; i++) {
			Cloud newCloud = new Cloud();
			newCloud.init(gc, true);
			Clouds.add(newCloud);
		}
		LastTimeCreated = getTime();
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		
		if(getTime()-LastTimeCreated > 500) {
			Cloud newCloud = new Cloud();
			newCloud.init(gc);
			Clouds.add(newCloud);
			LastTimeCreated = getTime();
		}

		if(getTime()-LastTimeCraftCreated > 1500) {
			Aircraft newCraft = new Aircraft(this);
			newCraft.init(gc);
			Aircrafts.add(newCraft);
			LastTimeCraftCreated = getTime();
		}
		
		if(Clouds.size() > 0) {
			for(int i = 0; i < Clouds.size(); i++ ) {
				Clouds.get(i).update(gc, delta);
				if(Clouds.get(i).getX() > 900) {
					Clouds.remove(i);
				}
			}
		}
		if(Aircrafts.size() > 0) {
			for(int i = 0; i < Aircrafts.size(); i++ ) {
				Aircrafts.get(i).update(gc, delta);
				if(Aircrafts.get(i).getX() > 900) {
					Aircrafts.remove(i);
				}else if(Aircrafts.get(i).getX() < -100) {
					Aircrafts.remove(i);
				}else if(Aircrafts.get(i).getY() > 700) {
					Aircrafts.remove(i);
					numberofkills++;
				}
			}
		}
		Trees.update(gc, delta);
		
	}
	
	public void render(GameContainer gc, Graphics g) throws SlickException {
		Background.draw();
		for(int i = 0; i < Aircrafts.size(); i++ ) {
			if(Aircrafts.get(i).getLayer() == 4) {
				Aircrafts.get(i).render(gc, g);
			}
		}
		for(int i = 0; i < Aircrafts.size(); i++ ) {
			if(Aircrafts.get(i).getLayer() == 3) {
				Aircrafts.get(i).render(gc, g);
			}
		}
		for(int i = 0; i < Clouds.size(); i++ ) {
			Clouds.get(i).render(gc, g);
		}
		for(int i = 0; i < Aircrafts.size(); i++ ) {
			if(Aircrafts.get(i).getLayer() == 2) {
				Aircrafts.get(i).render(gc, g);
			}
		}
		Trees.render(gc, g);
		for(int i = 0; i < Aircrafts.size(); i++ ) {
			if(Aircrafts.get(i).getLayer() == 1) {
				Aircrafts.get(i).render(gc, g);
			}
		}
	}
	
	private long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}

	public void leftClick(int hitx, int hity) {
		if(started) {
			for(int i = 0; i < Aircrafts.size(); i++ ) {
				Aircrafts.get(i).checkShot(hitx, hity, Sounds);
				if(this.checkMunition() >= 0) {
					Sounds.PlaySound("shot");
				}
			}
		}
	}

	public int checkMunition() {
		return Hud.getMunition();
	}
	
	public void addPoints(int points) {
		Hud.addPoints(points);
	}

	public void start() {
		started = true;
	}

	public void stop() {
		started = false;
	}

}
