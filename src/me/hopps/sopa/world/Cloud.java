package me.hopps.sopa.world;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Cloud {
	Image Cloud;
	double x, y;
	double speed;
	
	public Cloud() {
		
	}
	
	public void init(GameContainer gc) throws SlickException {
		Random rand = new Random();
		int img = rand.nextInt(3);
		if(img == 0) {
			img = 3;
		}
		Cloud = new Image("res/img/cloud"+img+".png");
		
		x -= Cloud.getWidth();
		y = rand.nextInt(190);
		speed = rand.nextDouble();
		if(speed < 0.16) {
			speed = 0.16;
		} else if(speed > 0.22) {
			speed = 0.22;
		}
	}
	
	public void init(GameContainer gc, boolean zufallsPosition) throws SlickException {
		Random rand = new Random();
		int img = rand.nextInt(2)+1;
		Cloud = new Image("res/img/cloud"+img+".png");
		
		x = rand.nextInt(800);
		y = rand.nextInt(190);
		speed = rand.nextDouble();
		if(speed < 0.2) {
			speed = 0.2;
		} else if(speed > 0.3) {
			speed = 0.3;
		}
	}

	public void update(GameContainer gc, int delta) throws SlickException {
		x += 0.1*delta/3.5;
	}
	
	public void render(GameContainer gc, Graphics g) throws SlickException {
		Cloud.draw(Math.round(x),Math.round(y));
	}

	public double getX() {
		return x;
	}
}
