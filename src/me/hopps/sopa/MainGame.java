package me.hopps.sopa;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

import me.hopps.sopa.world.World;

public class MainGame extends BasicGame{
	World World;
	Hud Hud;
	StartingScreen Start;
	SoundPlayer Sounds;
	int ONE, ONE2;
	
	public MainGame() {
		super("Crazy SOPA");	
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		
		gc.setVSync(true);
		Sounds = new SoundPlayer();
		ONE = 1;
		ONE2 = 1;
		Start = new StartingScreen();
		Hud = new Hud();
		World = new World(Hud, Sounds);
		
		
		Start.init(gc);
		Hud.init(gc);
		World.init(gc);
		Sounds.init(gc);
		
	}
	
	private void reinit(GameContainer gc) throws SlickException {
		ONE = 1;
		ONE2 = 1;
		Hud.reinit(gc);
		World.reinit(gc);
		Sounds.reloadMusic();
		Start.hidden = false;
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		if(gc.getInput().isMousePressed(org.newdawn.slick.Input.MOUSE_LEFT_BUTTON)) {
			Hud.shoot();
			World.leftClick(gc.getInput().getMouseX(), gc.getInput().getMouseY());
		}
		
		if(gc.getInput().isKeyPressed(org.newdawn.slick.Input.KEY_SPACE)) {
			Hud.reload();
		}
		if(gc.getInput().isKeyPressed(org.newdawn.slick.Input.KEY_S) && ONE == 1) {
			Sounds.PlaySound("start");
			Sounds.PlayMusic();
			Hud.start();
			World.start();
			Start.hide();
			ONE--;
		}
		if(Hud.getGameTime() <= 0) {
			World.stop();
			Hud.stop();
			Sounds.fadeOutMusic();
			if(ONE2 == 1) {
				Sounds.PlaySound("end");
				ONE2--;
			}
			
			if(gc.getInput().isKeyPressed(org.newdawn.slick.Input.KEY_R) && ONE == 0) {
				this.reinit(gc);
			}
			
		}
		
		Hud.update(gc, delta);
		World.update(gc, delta);
	}
	

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		World.render(gc, g);
		Hud.render(gc, g);
		Start.render(gc, g);
		
		if(Hud.getGameTime() <= 0) {
			Start.showWinningScreen(Hud.numberofshots, World.numberofkills, Hud.points);
		}
	}
	

}
