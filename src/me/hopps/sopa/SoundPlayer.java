package me.hopps.sopa;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class SoundPlayer {
	Sound end, exploision, shot, start;
	Music music;
	
	public SoundPlayer() {
		
	}
	
	public void init(GameContainer gc) throws SlickException {
		end = new Sound("res/sounds/end.wav");
		exploision = new Sound("res/sounds/exploision.wav");
		shot = new Sound("res/sounds/shot.wav");
		start = new Sound("res/sounds/start.wav");
		music = new Music("res/sounds/music.ogg");
	}
	
	public void PlaySound(String snd) {
		if(snd == "end") {
			end.play();
		} else if(snd == "exploision") {
			exploision.play();
		} else if(snd == "shot") {
			shot.play();
		} else if(snd == "start") {
			start.play();
		}
	}
	
	public void PlayMusic() {
		music.play(1f, 0.5f);
	}
	
	public void reloadMusic() throws SlickException {
		music = new Music("res/sounds/music.ogg");
	}

	public void fadeOutMusic() {
		music.fade(700, 0f, true);
	}

}
